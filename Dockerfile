# 
FROM python:3.9

# 
WORKDIR /code

# 
ADD ./requirements.txt /code/requirements.txt

# 
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

# 
ADD ./app /code/app

#
ADD ./posts /code/posts

# 
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8000", "--reload"]
