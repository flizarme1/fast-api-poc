create-network:
	docker network create db-connection || true

build-api:
	docker stop fast-api || true
	docker rm fast-api || true
	docker build -t fast-api .
	docker run -d --name fast-api -p 8000:8000 --network=db-connection -v `pwd`:/code fast-api

build-db:
	docker stop postgres || true
	docker rm postgres || true
	docker run --name postgres -e POSTGRES_PASSWORD=mysecretpassword -d --network=db-connection -p 5432:5432  postgres || true

create-stack:
	make create-network
	make build-db
	make build-api