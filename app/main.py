import time
from typing import Optional
from pathlib import Path
from fastapi import FastAPI, Depends, Request
from fastapi.middleware.cors import CORSMiddleware
from os import listdir, getcwd
from os.path import isfile, join
from sqlalchemy.orm import Session
from .sql_app import crud, models, schemas
from .sql_app.database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    response.headers["X-Process-Time"] = str(process_time)
    return response


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/posts/list")
def read_item():
    cwd = getcwd()
    files = [
        f.replace(".md", "")
        for f in listdir(f"{cwd}/posts/")
        if isfile(join(f"{cwd}/posts/", f))
    ]
    return {"files": files}


@app.get("/posts/{post_id}")
def read_item(post_id: str, db: Session = Depends(get_db)):
    return crud.get_post_by_name(db, post_id)
