from typing import List, Optional

from pydantic import BaseModel


class PostBase(BaseModel):
    body: str
    name: str


class Post(PostBase):
    id: int

    class Config:
        orm_mode = True


class PostCreate(PostBase):
    pass
