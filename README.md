## Note

Start with:
```
make create-stack
```

If you already have a Postgres database locally for other uses, update the port number in the Makefile to something other than 5432. For example (`5435`), `docker run --name postgres -e POSTGRES_PASSWORD=mysecretpassword -d --network=db-connection -p 5435:5432  postgres || true`.

Once the database is running use pgAdmin to connect to it. DB Name: `postgres` Username: `postgres` Password: `mysecretpassword` Host: `localhost` Post: `5432`

From here, drill into your public folder and create a new `posts` table. The table has three columns: `id`, `body` and `name`. The `id` will be the primary key. The `body` will be the entire text body from the Next.js blog post examples (from the nextjs repo). The `name` will be the name of the post/file (for example `ssg-ssr`). Manually add the two posts from the nextjs example.

## Resources 

[FastAPI Tutorial](https://fastapi.tiangolo.com/tutorial/)  
[pgAdmin](https://www.pgadmin.org/)
